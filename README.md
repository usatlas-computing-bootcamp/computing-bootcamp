# Description
This is a repository that couples the development of resource as well as the
deployment of documentation to be used to conduct educational interventions
for HEP training.  This training can be experiment agnostic, meaning that 
no single experiments standard practices are tied to the lessons, or can be
experiment specific which must be tailored by members of that experiment and
are not as generally pertinent.


# Contributing
There are two way to contribute to this repository, and both are very welcome :
  - **Merge requests** : Welcome and will be reviewed and revised and then incorporated when appropriate.
  - **Issues** : If you have an idea that you think may be useful to incorporate, but don't have time immediately, feel free to file an issue to its saved for the future.


# Contact Names
The following people are somehow tied to this project and can be contacted with
questions or ideas :
  - Samuel Meehan <samuel.meehan@cern.ch>