# Introduction
Welcome to HEP and ATLAS!  Maybe you are new and maybe you have been a part of ATLAS or 
another collaboration for many years.  In any case, learning is always good, so we are glad
you found your way here.  The computing skills to be an effective 
scientist are continually evolving and although many resources exist outside of our community
by which you can self-educate, our aim is to provide an efficient means by which to acquire the
most essential skills that are central to our field in particular.  

These skills can roughly be factorized into two categories 

  - [*Experimental Agnostic*](experiment_agnostic/index.md) : Concepts, techniques and tools that are common
to all fields of HEP that rely on computing as well as specific applications to our experiment.
These resources are being developed in conjunction with the [First-HEP](https://first-hep.org/)
group and aim to evolve over time and expand to include things that may not immediately pertain
to topics pertinent to ATLAS.

  - [*ATLAS Specific*](atlas/index.md) : These skills are intrinsically tied to the workflows and norms that have
been developed within the collaboration and which will be assumed knowledge throughout
your day to day work with others.





