# Introduction
This repository is for developing and documenting resources used for training 
individuals new to HEP and ATLAS in particular.  These tools are being developed in 
collaboration with [First-HEP](https://first-hep.org/) so as to make them sustainable.

# Contributing
Feel free to contribute by openning a merge request to make modifications of any type. If
your merge request is not attended to promptly, do not hesitate contacting one of the 
individuals responsible for these tools.

## Module Templates
If you are looking to develop a module, then there is a philosophy to make things appear 
uniformly pedagogical.  The general approach is roughly tailored after the lessons
developed by [Software Carpenty](https://software-carpentry.org/lessons/).  Adhering
to this uniform template will ensure that a participant knows what to expect.  If you feel
that the template should be module template should be modified, then feel free to open
an issue or a merge request, noting that this will require modifications to existing lessons, 
but that's not a problem since we are not lazy, right?

The module template can be found here : [module_template.md](module_template.md)

# How did you create this?
This documentation was created with [MkDocs](https://www.mkdocs.org/) following the example and procedure outlined by [CERN-IT here]('https://gitlab.cern.ch/authoring/documentation/mkdocs').

# To Do List
This is still under construction and we should do the following
- House the site on a service account