# Version Control
Broad high level introduction with perhaps broad philosophical statements or perhaps
rhetorical questions, because those get people warmed up and thinking, right?  This does 
not have to be succinct and point to specific aspects of the lesson.  Some people may 
just skip reading it.  However, we want to motivate them to realize that what they are
learning is a transferable skill.

# What You Are Getting

**Description** : What is the one sentence description or this lesson

**Learning Goal** : What is the specific skill that will be learned?  What will they know how
to do, or to do *properly*, which they previously may not have known?

**Activity** : What will they be doing themselves, by typing on a keyboard?

# The Lesson
This is the longest part of the module and should be self-contained to the extent that 
we can work through it in real time with the students, but they can also follow it themselves
after the fact.  We don't want just a bunch of copy/paste commands, but a mix of pedantic
text interspersed with actual "now do this" portions.

# External Resources
Here we can provide a list of links to resources that go beyond what we have created.
These are likely the things that already exist, but not in a single uniform and standardized
format that follows a consistent pedagogy as developed here.